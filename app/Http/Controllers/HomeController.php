<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function search()
    {
        $foo = file_get_contents('http://82.223.27.125:8983/solr/D1/select?q=*%3A*&wt=json&indent=true');
        $fa = json_decode($foo);
        return view('home')->with('foo',$fa);
    }
    public function store(Request $request, $id ='0')
    {
       // $name = $request->input('searchBar');
        $url= 'http://82.223.27.125:8983/solr/D1/select?q='.$id.'&start=0&rows=5&wt=json&indent=true';
        $foo = file_get_contents($url);
        $fa = json_decode($foo);
      // return view('home')->with('fa',$fa);
        return [$fa];
    }
    public function store_checkbox(Request $request)
    {
         $input = Input::all();
         $fq="";
        foreach ($input['check'] as $value)
        {
            $fq.= "&fq=".$value;
        }
        // $name = $request->input('searchBar');

        $url= 'http://82.223.27.125:8983/solr/D1/select?q='.$input['search_key'].$fq/*.'&start='.$input['start'].'&rows='.$input['rows']*/.'&wt=json&indent=true';
       // $foo = file_get_contents($url);
       // $fa = json_decode($foo);
        // return view('home')->with('fa',$fa);
        $foo = file_get_contents($url);
        $fa = json_decode($foo);
        // return view('home')->with('fa',$fa);
        return [$fa];
    }
}
