
## About DREAM

DREAM is a multi-application content management platform founded in 2012 by Business-index and is evolving continously. It can automatically index a document according to several facets meeting different business needs. It thus avoids redundancy and improves the relevance of keyword searches. It can enrich an intranet or a web portal and improve seeking information.

Dream is developpement use the followed technologies:
- [Blazing-fast, open source enterprise search platform ](http://lucene.apache.org/solr/).
- [MiddleWare Getting the Apis results LARAVEL](https://laravel.com/docs/container).
- Multiple front-ends for view [Javascript](https://vuejs.org/) and [Css](https://almsaeedstudio.com/themes/AdminLTE/index2.html) .

![alt tag](http://i.imgur.com/Et6crMg.png "Main Page")
![alt tag](http://i.imgur.com/rAAGlTW.png "Application Page")
## Learning Laravel

Laravel has the most extensive and thorough documentation and video tutorial library of any modern web application framework. The [Laravel documentation](https://laravel.com/docs) is thorough, complete, and makes it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 900 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

####Installation
1. Laravel Use [Composer](https://getcomposer.org/download/) So you should make sure that composer is "Globally" Installed in your computer.
2. Git clone this project (If you are using MAMP, WAMP or LAMP you will have to clone in your projects folder).
3. Go to root project and type in a terminal "composer install && npm install".
4. Open the .env file in the root project. And Edit it. Configure your database connection as below.
###
    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=dream_database
    DB_USERNAME=root
    DB_PASSWORD=
###
5. Again in the root project folder type in the terminal "php artisan serve" to launch the project.
## Contributing
Soon a documentation API...
## License

This Project has been developped and is property of Bussiness Index [Copyright license](http://www.business-index.fr/).
