
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import Vue from 'vue'
import App from './App.vue'

require('./bootstrap');
$(document).ready(function(){
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-red ',
        increaseArea: '20%' // optional
    });
});
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('search', require('./components/Search.vue'));
Vue.component('result', require('./components/Result.vue'));
Vue.component('layout-result', require('./components/result.layouts/Result_layout.vue'));
Vue.component('side',require('./components/Side.vue'));
const app = new Vue({
    el: '#dream',
    render: h => h(App)
});
