<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Input;
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('search',function(){
    $foo = file_get_contents('http://82.223.27.125:8983/solr/D1/select?q=*%3A*&wt=json&indent=true');
    $fa = json_decode($foo);
    return [$fa];
});

Route::get('postsearch/{id}','HomeController@store');
Route::get('postcheck','HomeController@store_checkbox');
