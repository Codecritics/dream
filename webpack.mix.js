const {mix} = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.webpackConfig({
    output: {
        path: __dirname + "/public"
    }
});

mix.js('resources/assets/js/app3.js', 'public/js').extract(['vue', 'jquery', 'admin-lte']);
mix.sass('resources/assets/sass/app.scss', 'public/css');
//mix.less('resources/assets/less/app.less', 'public/css/less.css');
if (mix.config.inProduction) {
    mix.version();
}